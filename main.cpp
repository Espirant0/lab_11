﻿#include <iostream>
#include "data.hpp"
#include <vector>

using namespace saw;

int main()
{
	setlocale(LC_ALL, "Rus");
	std::vector<Device*> vec;
	Device* device = new Device("Reallab NL16HV", 0);
	vec.push_back(device);
	device = new Device("ЭНМВ-1-24", 1); 
	vec.push_back(device);	

	for (const auto& i : vec)
	{
		i->Poll();
		std::cout << " " << std::endl;
		i->Print();
	}
	for (int i = 0; i < vec.size(); i++)
	{
		delete vec[i];
	}
}

