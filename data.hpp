#pragma once
#include <iostream>

namespace saw
{
	class Device
	{
		int m_address;
		std::string m_name;
		bool input1, input2;
	public:
		Device(std::string name, int address);
		~Device();
		void Poll();
		void Print();
	};
}